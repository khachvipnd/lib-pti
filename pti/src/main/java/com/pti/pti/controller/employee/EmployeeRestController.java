package com.pti.pti.controller.employee;

/*
 * Author: Nguyen The Anh 
 * Email: anhnt1@pti.com.vn
 */

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
//target: test res controller
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pti.lib.entities.Employee;
import com.pti.lib.service.EmployeeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController 
@RequestMapping("/rest")
@Api( tags = "Employee")
public class EmployeeRestController {

	@Autowired
	private EmployeeService employeeService;
	

	/**
	 * list all of employee
	 */
	@ApiOperation(value = "This method is used to get the clients.")
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Employee item found"),
	        @ApiResponse(code = 404, message = "Employee not found")})
	@RequestMapping(value ="/employees", 
			method = RequestMethod.GET, 
			produces= {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public List<Employee> getEmployees(){
		List<Employee> list = employeeService.getAllEmployee();
		return list;
	}
	
	
	/**
	 * get detail of Employee
	 * 
	 */
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Employee item found"),
	        @ApiResponse(code = 401, message = "Employee not exist"),
	        @ApiResponse(code = 404, message = "Employee not found")})
	@ApiOperation(value = "This method is used to get detail of client.")
	@RequestMapping(value="/employees/{empId}", 
			method = RequestMethod.GET, 
			produces= {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public Employee getEmployee(@PathVariable("empId") Long empId) {
		
		return employeeService.getEmployee(empId);
	}
	
	/**
	 * Add new Employee
	 */
	@ApiOperation(value = "This method is used to add more an employee.")
	@RequestMapping(value ="/employees", 
			method = RequestMethod.POST, 
			produces= {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public Employee addEmployee(@RequestBody Employee empForm) {
		System.out.println("(Server side) Creating new Employee with empNo: "+ empForm.getEmpName());
		return employeeService.addEmployee(empForm);
	}
	
	
	/**
	 * Update Employee
	 */
	@ApiOperation(value = "This method is used to update information of an employee.")
	@RequestMapping(value ="/employees", 
			method = RequestMethod.PUT, 
			produces= {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public Employee updateEmployee(@RequestBody Employee empForm) {
		System.out.println("(Server side) Editing new Employee with empNo: "+ empForm.getEmpName());
		return employeeService.updateEmployee( empForm);
	}
	
	/**
	 * Delete Employee
	 */
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Employee item found"),
	        @ApiResponse(code = 404, message = "Employee not found")})
	@RequestMapping(value ="/employees/{empId}", 
			method = RequestMethod.DELETE, 
			produces= {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public String deleteEmployee(@PathVariable("empId") Long empId) {
		System.out.println("(Server side) Deleting Employee with empNo: "+ empId);
		employeeService.deleteEmployee( empId);
		return "Delete success";
	}
	
}
