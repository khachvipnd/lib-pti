package com.pti.pti.controller;

/*
 * Author: Nguyen The Anh 
 * Email: anhnt1@pti.com.vn
 */

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MainController {
	@RequestMapping("/")
	@ResponseBody
	public String welcome() {
		return "Welcome to PTI Back End";
	}
	@RequestMapping("/home")
	public String index() {
		return "index";
	}
}
