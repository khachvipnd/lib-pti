package com.pti.pti.controller.user;

/*
 * Author: Nguyen The Anh 
 * Email: anhnt1@pti.com.vn
 */

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pti.lib.entities.User;
import com.pti.lib.service.JwtService;
import com.pti.lib.service.UserService;


@RestController
@RequestMapping("/rest")
@ComponentScan({"com.pti.lib.*"})
public class UserRestController {
	
  @Autowired
  private JwtService jwtService;
  @Autowired
  private UserService userService;

  @RequestMapping(value = "/login", method = RequestMethod.POST)
  public ResponseEntity<String> login(HttpServletRequest request, @RequestBody User user) {
    String result = "";
    HttpStatus httpStatus = null;
    try {
      if (userService.checkLogin(user)) {
        result = jwtService.generateTokenLogin(user.getUsername());
        httpStatus = HttpStatus.OK;
      } else {
        result = "Wrong userId and password";
        httpStatus = HttpStatus.BAD_REQUEST;
      }
    } catch (Exception ex) {
      result = ex.toString();
      httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    }
    return new ResponseEntity<String>(result, httpStatus);
  }
  
  
}