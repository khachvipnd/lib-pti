Link: https://www.youtube.com/watch?v=_P9zsZQtMKE&list=PLpsUNSYqSiqtFmoQUSqsgJv75KYLIsPv9&index=21

Các chú ý:
1/ trong loadUserByUsername và checkLogin, khi viết cau lệnh query với session.createQuery(hql), 
	trong đó hql ="from User where username = :username and password = :password";
	phải viết tên bảng là tên class model trong code, không phải là tên trong table trong db
2/ .access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	 với role này, cần chú ý vào trường RoleName trong db
	 
3/ trong file pom.xml dùng  version 2.2.8.RELEASE để tránh lỗi securityConfig
<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.2.8.RELEASE</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>	 
	 
4. @ResponseBody: để trả về string sau từ khóa return, nếu ko có, thì nó sẽ gọi đến file trong templates folder	 

5/ khi gặp lỗi  java.lang.IllegalArgumentException: org.hibernate.hql.internal.ast.QuerySyntaxException: User is not mapped
vào USERDAO.java và tới dòng: Query query = session.createQuery(hql);
chuyển thành : 
Query query = session.createQuery(hql, User.class);
(thêm  User.class, để ánh xạ)
 ngoài ra thì cần set config ở file application của project pti để đọc config hibernate cho việc loading mapping 

6/ chú ý , muốn sử dụng thuộc tính strategy = GenerationType.IDENTITY
thì bảng đó trong Database phải set là identify,(refer đến bảng Employees )


7/ cách export lib project
 chuột phải vào project => chọn Export => chọn Java(jar file)
 => chọn Export generated class file and resources và Export refactoring for checked projects,(refer ảnh attached)
 => sau đó Next => Finish
 => import file jar vào project cần( Chuột phải vào project cần import => Build Path => Add External Archived)
 
============
Hướng dẫn chạy (có 2 case, user và admin)
1. vào postman run curl sau:
	1.1: user:
	curl --location --request POST 'http://localhost:8080/rest/login' \
--header 'Content-Type: application/json' \
--header 'Cookie: JSESSIONID=9EE21C4CCF07B137F86D27CA1A0D1D06' \
--data-raw '{
    "username" :"user",
    "password" :"user"
}'
	1.2 : admin account:
	curl --location --request POST 'http://localhost:8080/rest/login' \
--header 'Content-Type: application/json' \
--header 'Cookie: JSESSIONID=9EE21C4CCF07B137F86D27CA1A0D1D06' \
--data-raw '{
    "username" :"admin",
    "password" :"admin"
}'

2. chạy các phương thức, get, post, update( với token được sinh ra ở bước 1):
	curl --location --request GET 'http://localhost:8080/rest/employees/' \
--header 'Authorization: eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NjYyNjE5ODMsInVzZXJuYW1lIjoiYWRtaW4ifQ.mA0DpIDtWXWvZqrzvlZY_j-Rp1STyuO3zP6rvLw6qng' \
--header 'Content-Type: application/json' \
--header 'Cookie: JSESSIONID=9EE21C4CCF07B137F86D27CA1A0D1D06' \
--data-raw ' {
        "empId": 6,
        "empNo": "EMP006",
        "empName": "Test 6"
    }'
    
    ===
    chạy trên BROWSER:
    
    1: truy cập:
     http://localhost:8080/swagger-ui.html#/user-rest-controller
    2. vào user-rest-controller, và login với body truyền vào
		    {
		  "password": "admin",
		  "rolename": "ROLE_ADMIN",
		  "username": "admin"
			}
	3. lấy token
	4. kéo lên trên cùng, click vào button Authorize, và insert token này vào
	5. thao tác với các method bình thường
	