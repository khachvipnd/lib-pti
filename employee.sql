--------------------------------------------------------
--  File created - Wednesday-October-26-2022   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table EMPLOYEESS
--------------------------------------------------------

  CREATE TABLE "SYS"."EMPLOYEESS" 
   (	"EMPID2" NUMBER(*,0), 
	"EMPNO" VARCHAR2(20 BYTE), 
	"EMPNAME" VARCHAR2(20 BYTE), 
	"EMPID" NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE 
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
REM INSERTING into SYS.EMPLOYEESS
SET DEFINE OFF;
Insert into SYS.EMPLOYEESS (EMPID2,EMPNO,EMPNAME,EMPID) values (null,'Emp 7','Emp7',7);
Insert into SYS.EMPLOYEESS (EMPID2,EMPNO,EMPNAME,EMPID) values (null,'8','8',8);
Insert into SYS.EMPLOYEESS (EMPID2,EMPNO,EMPNAME,EMPID) values (null,'Emp 02','Emp02',5);
--------------------------------------------------------
--  Constraints for Table EMPLOYEESS
--------------------------------------------------------

  ALTER TABLE "SYS"."EMPLOYEESS" MODIFY ("EMPID" NOT NULL ENABLE);
