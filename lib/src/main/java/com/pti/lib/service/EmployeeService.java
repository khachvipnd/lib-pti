package com.pti.lib.service;

/*
 * Author: Nguyen The Anh 
 * Email: anhnt1@pti.com.vn
 */

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pti.lib.dao.EmployeeDAO;
import com.pti.lib.entities.Employee;


@Service
@Transactional

public class EmployeeService {
	@Autowired
	private EmployeeDAO employeeDAO;
	@Autowired
	private SessionFactory sessionFactory;
	
	public Employee getEmployee( Long empId) {
		Session session = sessionFactory.getCurrentSession();
		return employeeDAO.getEmployee(session, empId);
	}
	
	public Employee addEmployee( Employee empForm) {
		Session session = sessionFactory.getCurrentSession();
		return employeeDAO.addEmployee(session, empForm);
	}
	
	public Employee updateEmployee( Employee empForm) {
		Session session = sessionFactory.getCurrentSession();
		return employeeDAO.updateEmployee(session, empForm);
	}
	
	public void deleteEmployee( Long empId) {
		Session session = sessionFactory.getCurrentSession();
		employeeDAO.deleteEmployee(session, empId);
	}
	
	public List<Employee> getAllEmployee() {
		Session session = sessionFactory.getCurrentSession();
		return employeeDAO.getAllEmployees(session);
	}
	
}
