package com.pti.lib.service;

/*
 * Author: Nguyen The Anh 
 * Email: anhnt1@pti.com.vn
 */


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pti.lib.dao.UserDAO;
import com.pti.lib.entities.User;



@Service
@Transactional
public class UserService {
	@Autowired
	private UserDAO userDao;
	
	public User loadUserByUsername(String username) {
		return userDao.loadUserByUsername(username);
	}
	
	public boolean checkLogin(User user) {
		return userDao.checkLogin(user);
	}
	
	
}
