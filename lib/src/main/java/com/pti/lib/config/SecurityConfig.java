package com.pti.lib.config;

/*
 * Author: Nguyen The Anh 
 * Email: anhnt1@pti.com.vn
 */

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.pti.lib.rest.CustomAccessDeniedHandler;
import com.pti.lib.rest.JwtAuthenticationTokenFilter;
import com.pti.lib.rest.RestAuthenticationEntryPoint;


@EnableWebSecurity
@Configuration
@Component
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	private static final String[] AUTH_LIST = {
	        // -- swagger ui
	        "**/swagger-resources/**",
	        "/swagger-ui.html",
	        "/v2/api-docs",
	        "/webjars/**"
	};
	
  @Bean
  public JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter() throws Exception {
    JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter = new JwtAuthenticationTokenFilter();
    jwtAuthenticationTokenFilter.setAuthenticationManager(authenticationManager());
    return jwtAuthenticationTokenFilter;
  }
  
  @Bean
  public RestAuthenticationEntryPoint restServicesEntryPoint() {
    return new RestAuthenticationEntryPoint();
  }
  
  @Bean
  public CustomAccessDeniedHandler customAccessDeniedHandler() {
    return new CustomAccessDeniedHandler();
  }
  
  @Bean
  @Override
  protected AuthenticationManager authenticationManager() throws Exception {
    return super.authenticationManager();
  }
  
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/static/css/**, /static/js/**, *.ico");
		
		// swagger
		web.ignoring().antMatchers(
		    		 "/v2/api-docs",  "/configuration/ui",
           "/swagger-resources", "/configuration/security",
           "/swagger-ui.html", "/webjars/**","/swagger/**");
	}
	
  @Override
  protected void configure(HttpSecurity http) throws Exception {
	http.headers().frameOptions().disable();
    http.csrf().ignoringAntMatchers("/rest/**");
    http
    .authorizeRequests().antMatchers(AUTH_LIST).authenticated()
    .and()
    .httpBasic().authenticationEntryPoint(swaggerAuthenticationEntryPoint())
    .and()
    .csrf().disable();
    http.authorizeRequests().antMatchers("/rest/login**").permitAll();
    http.authorizeRequests().antMatchers("/swagger-resources/**").permitAll();
    http.antMatcher("/rest/**").httpBasic().authenticationEntryPoint(restServicesEntryPoint()).and()
    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
    .antMatchers(HttpMethod.GET, "/rest/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    .antMatchers(HttpMethod.POST, "/rest/**").access("hasRole('ROLE_ADMIN')")
    .antMatchers(HttpMethod.DELETE, "/rest/**").access("hasRole('ROLE_ADMIN')").and()
    .addFilterBefore(jwtAuthenticationTokenFilter(), UsernamePasswordAuthenticationFilter.class)
    .exceptionHandling().accessDeniedHandler(customAccessDeniedHandler());
    
  }
  @Bean
  public BasicAuthenticationEntryPoint swaggerAuthenticationEntryPoint() {
      BasicAuthenticationEntryPoint entryPoint = new BasicAuthenticationEntryPoint();
      entryPoint.setRealmName("Swagger Realm");
      return entryPoint;
  }
}