package com.pti.lib;

/*
 * Author: Nguyen The Anh 
 * Email: anhnt1@pti.com.vn
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LibApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibApplication.class, args);
	}

}
