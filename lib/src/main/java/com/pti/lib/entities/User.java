package com.pti.lib.entities;

/*
 * Author: Nguyen The Anh 
 * Email: anhnt1@pti.com.vn
 */

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

//import com.fasterxml.jackson.annotation.JsonInclude;
//
//@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@Table(name = "USERS")
public class User implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@SequenceGenerator( name = "USER_ID_SEQ", sequenceName  = "USER_ID_SEQ", allocationSize = 1)
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_ID_SEQ")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false,  precision = 3, scale = 0)
	private Integer id;
	private String username;
	private String password;
	private String rolename;
	

	public User() {

	}
	public User(Integer id, String username, String password, String rolename) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.rolename = rolename;
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="USERNAME", unique=true)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	@Column(name="PASSWORD", unique=false )
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name="ROLENAME", unique=false, length=20)
	public String getRolename() {
		return rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}


	@Transient
	public List<GrantedAuthority> getAuthorities() {
	    List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
	    authorities.add(new SimpleGrantedAuthority(getRolename()));
	    return authorities;
	  }

	
}
