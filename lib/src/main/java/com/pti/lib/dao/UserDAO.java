package com.pti.lib.dao;


/*
 * Author: Nguyen The Anh 
 * Email: anhnt1@pti.com.vn
 */


import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pti.lib.entities.User;


@Repository
public class UserDAO {
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@SuppressWarnings({"unchecked", "rawtypes"})
	public User loadUserByUsername(final String username) {
		Session session = this.sessionFactory.getCurrentSession();
		String hql ="from User where username =:username";
		Query query = session.createQuery(hql);
		query.setParameter("username", username);
		List<User> users = query.list();
		if(users != null && users.size()> 0 ) {
			return users.get(0);
		}else {
			return null;
		}
	}
	
	@SuppressWarnings({"unchecked","rawtypes"})
	public boolean checkLogin(User user) {
		Session session = this.sessionFactory.getCurrentSession();
		
		String hql ="from User where Username = :username and Password = :password";
		Query query = session.createQuery(hql, User.class);
		query.setParameter("username", user.getUsername());
		query.setParameter("password", user.getPassword());
		List<User> users = query.list();

		if(users != null && users.size()> 0 ) {
			return true;
		}else {
			return false;
		}
	}
	
}
