package com.pti.lib.dao;


/*
 * Author: Nguyen The Anh 
 * Email: anhnt1@pti.com.vn
 */


import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.pti.lib.entities.Employee;


@Repository
public class EmployeeDAO {
	
	public Employee getEmployee(Session session,Long empId) {
		
		Employee emp= (Employee) session.get(Employee.class, empId);
		return emp;
	}
	
	public Employee addEmployee(Session session,Employee empForm) {
		session.save(empForm);
		return empForm;
	}
	
	public Employee updateEmployee(Session session,Employee empForm) {
		Employee emp = (Employee) session.get(Employee.class, empForm.getEmpId());
		emp.setEmpName(empForm.getEmpName());
		emp.setEmpNo(empForm.getEmpNo());
		session.update(emp);
		return emp;
		
	}
	
	public void deleteEmployee(Session session,Long empId) {
		Employee emp = (Employee) session.get(Employee.class,empId);
		session.delete(emp);
	}
	
	public List<Employee> getAllEmployees(Session session){
		@SuppressWarnings("unchecked")
		List<Employee> listEmployee = (List<Employee>) session.createQuery("FROM "+Employee.class.getName()).list();
		return listEmployee;
	}
	
}
