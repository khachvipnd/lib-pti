--------------------------------------------------------
--  File created - Wednesday-October-26-2022   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table USERS
--------------------------------------------------------

  CREATE TABLE "SYS"."USERS" 
   (	"ID" NUMBER, 
	"USERNAME" NVARCHAR2(20), 
	"PASSWORD" NVARCHAR2(40), 
	"ROLENAME" NVARCHAR2(20)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
REM INSERTING into SYS.USERS
SET DEFINE OFF;
Insert into SYS.USERS (ID,USERNAME,PASSWORD,ROLENAME) values (2,'user','user','ROLE_USER');
Insert into SYS.USERS (ID,USERNAME,PASSWORD,ROLENAME) values (1,'admin','admin','ROLE_ADMIN');
